#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-only
"""
This program provides a function not possible with gpg:
Exporting a secret key unencrypted (in the clear).

Tested with gpg version 2.2.40


By Max-Julian Pogner <max-julian@pogner.at>, 2022


Note 2022: I discovered also a related program here:
https://github.com/pts/gpg-export-secret-key-unprotected

"""
##
## Note: functions are only used for folding grouped lines of code
## read as if code in function is one continous flow.
##

import argparse
import logging
import pathlib
import shutil
import subprocess
import sys
import tempfile

from contextlib import  contextmanager


if "__main__" == __name__:
    LOGFORMAT = '%(module)s:%(lineno)d %(levelname)s: %(message)s'
    logging.basicConfig(format=LOGFORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser(
    description=__doc__)
parser.add_argument("--armor",
    help="If specified, the key is output in armored encoding.",
    action='store_true',
    default=False)
parser.add_argument("KEY_SPEC",
    help="A key search specification understood by gpg. For example "
        "the full key fingerprint.")
args = parser.parse_args()


def get_gpginfo(homedir=None):
    """
    get some information on the gpg environment.
    use given homedir, or default homedir if None given.
    """
    gpginfo = {}
    sp = subprocess.run(
        ("gpgconf", "--list-dirs"),
        check=True, capture_output=True, text=True)
    for line in sp.stdout.split("\n"):
        if line.startswith("homedir:"):
            gpginfo["homedir"] = line.split(":", 1)[1]
    return gpginfo


MAIN_GPGINFO = get_gpginfo(homedir=None)


def parse_keyinfo(gpgstdout):
    """
    convert the gpg --with-colon format into dictionary
    """
    keyinfo = {}
    for line in gpgstdout.split("\n"):
        if line.startswith("grp:"):
            keyinfo["keygrip"] = line.split(":")[9]
        elif line.startswith("fpr:"):
            keyinfo["fingerprint"] = line.split(":")[9]
        elif line.startswith("uid:"):
            keyinfo["name"] = line.split(":")[9]
    return keyinfo


def find_single_keyinfo_or_exit(key_spec):
    """
    acquire list of all the keys matching the args.KEY_SPEC
    then check if only one element in list, and parse that element.
    """
    sp = subprocess.run(
        ("gpg", "--batch", "--list-secret-keys", "--with-colon",
            "--with-keygrip", key_spec),
        capture_output=True, text=True)
    if 0 != sp.returncode:
        logger.error("No secret key found for given key "
            "specification: '%s'", args.KEY_SPEC)
        sys.exit(1)
    count = 0
    for line in sp.stdout.split("\n"):
        if line.startswith("grp:"):
            count += 1
    if 1 != count:
        logger.error("Too many secret keys found for given key "
            "specification: '%s'", args.KEY_SPEC)
        sys.exit(1)
    keyinfo = parse_keyinfo(sp.stdout)
    return keyinfo


KEYINFO = find_single_keyinfo_or_exit(args.KEY_SPEC)
KEYGRIP = KEYINFO["keygrip"]


def loginfo_key_to_export(key_spec):
    logger.info("exporting the following key:")
    sp = subprocess.run(
        ("gpg", "--batch", "--list-secret-keys", "--with-keygrip",
            key_spec),
        capture_output=True, text=True)
    for line in sp.stdout.rstrip().split("\n"):
        logger.info(line)
    logger.info("")


loginfo_key_to_export(f"&{KEYGRIP}")


def get_passphrase_from_user(keyinfo):
    # get the old passphrase from user
    pinentry_commands = "\n".join((
        "SETDESC Please give the passphrase for exporting the secret "
            "(or press Enter if no passphrase):%0A"
            f"{keyinfo['name']}%0A"
            f"Fingerprint {keyinfo['fingerprint']}",
        "SETPROMPT Passphrase:",
        "GETPIN",
        "BYE"))
    sp = subprocess.run(("/usr/bin/pinentry",),
        text=True, input=pinentry_commands,
        capture_output=True,
        check=True)
    passphrase = None
    for line in sp.stdout.split("\n"):
        if line.startswith("D "):
            passphrase = line.split(" ", 1)[1]
    return passphrase


PASSPHRASE = get_passphrase_from_user(KEYINFO)


def write_custom_pinentry(outfile : pathlib.Path, oldpassphrase : str):
    outfile.write_text(r"""#!/usr/bin/python3
import os
import sys
import io
infos = {
    "flavor": "stub",
    "version": "1.20221119.0",
    "ttyinfo": "- - -",
    "pid": str(os.getpid()),
}
next_secret = None
secrets = {
    "old": """ + repr(oldpassphrase) + r""",
    "new": "",
}
sys.stdout = io.TextIOWrapper(sys.stdout.detach().detach(), write_through=True)
sys.stdout.write("OK Ready\n")
while True:
    line = sys.stdin.readline().rstrip()
    if not line:
        break
    elif line.startswith("GETINFO "):
        rest = line.split(" ", 1)[1]
        sys.stdout.write(f"D {infos[rest]}\n")
        sys.stdout.write("OK\n")
    elif line.startswith("SETDESC Please enter the passphrase to unlock the OpenPGP secret key:"):
        next_secret = "old"
        sys.stdout.write("OK\n")
    elif line.startswith("SETDESC Please enter the new passphrase"):
        next_secret = "new"
        sys.stdout.write("OK\n")
    elif "GETPIN" == line:
        sys.stdout.write(f"D {secrets[next_secret]}\n")
        sys.stdout.write("OK\n")
    elif "BYE" == line:
        break
    else:
        sys.stdout.write("OK\n")
""")


@contextmanager
def mktempdirpath():
    """
    temporary directory that is cleared after usage
    """
    # TODO: better make a ramdisk, so cleartext key is never put onto disk?
    dirpath = tempfile.mkdtemp()
    try:
        yield pathlib.Path(dirpath)
    finally:
        shutil.rmtree(dirpath)


with mktempdirpath() as tempdirpath:
    tempdirpath.joinpath("gpg-agent.conf").write_text(
        f"pinentry-program {str(tempdirpath)}/my-pinentry")
    mypinentry = tempdirpath.joinpath("my-pinentry")
    write_custom_pinentry(mypinentry, PASSPHRASE)
    mypinentry.chmod(0o700)

    logger.info("Importing public and private key into temporary gnupg home %s", tempdirpath)
    enckey_proc = subprocess.run(
        ("gpg", "--batch", "--export", f"&{KEYGRIP}"),
        capture_output=True, check=True)
    subprocess.run(
        ("gpg", "--batch", "--homedir", str(tempdirpath), "--import"),
        check=True, input=enckey_proc.stdout)
    tempprivkeyfile = tempdirpath.joinpath("private-keys-v1.d", f"{KEYGRIP}.key")
    homeprivkeyfile = pathlib.Path(MAIN_GPGINFO["homedir"]).joinpath(
            "private-keys-v1.d", f"{KEYGRIP}.key")
    tempprivkeyfile.write_bytes(
        homeprivkeyfile.read_bytes())

    logger.info("Removing passphrase of secret key")
    subprocess.run(("gpg", "--batch", "--homedir", str(tempdirpath),
        "--no-greeting", "--change-passphrase", f"&{KEYGRIP}"),
        check=True, capture_output=True, text=True)
    logger.info("Exporting secret key as cleartext (armored)")
    if args.armor:
        opt_armor = "--armor"
    else:
        opt_armor = "--no-armor"
    sp = subprocess.run(("gpg", "--batch", "--homedir", str(tempdirpath),
        "--export-secret-key", opt_armor, f"&{KEYGRIP}"),
        capture_output=True)
    if 0 != sp.returncode:
        logger.error(sp.stderr)
        sys.exit(1)
    sys.stdout.buffer.raw.write(sp.stdout)
    sys.stdout.flush()
