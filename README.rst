

*******************
 My gpg2 utilities
*******************


* **gpg-multiclearsign.sh** ... *gpg2 --clearsign* a textfile (binary may not
  be supportable) multiple times with different keys.

  Example usage: Two persons want to sign a contract.txt

  Each person can verify both signatures with vanilla *gpg2 --verify*

* **gpg-export-secret-key-cleartext.py** ... *gpg --export-secret-key*, but
  with the output containined the secrets in the clear.

  Example usage: gpg-export-secret-key-cleartext.py --armor 0xKEYFINGERPRINT
